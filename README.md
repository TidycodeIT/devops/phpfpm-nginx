## Tidycode Docker Registry
- https://gitlab.com/TidycodeIT/devops/phpfpm-nginx/container_registry

## Supported images
- `phpfpm-nginx/magento:php-7.2`
- `phpfpm-nginx/magento:php-7.3`
- `phpfpm-nginx/magento:php-8.2`

## Example Usage
- `docker run registry.gitlab.com/tidycodeit/devops/phpfpm-nginx/magento:php-7.2`

## Customizing Nginx

Add custom files here if you want to customize the nginx configurations:

- `/var/www/html/conf/nginx/nginx-site.conf`
- `/var/www/html/conf/nginx/nginx.conf`

## References

This repository has been inspired by https://github.com/jkaninda/nginx-php-fpm
